package com.greatlearning.manymanybi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManyManyBiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManyManyBiApplication.class, args);
    }

}
