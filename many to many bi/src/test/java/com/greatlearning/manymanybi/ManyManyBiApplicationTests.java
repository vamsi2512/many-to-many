package com.greatlearning.manymanybi;

import com.greatlearning.manymanybi.entity.Role;
import com.greatlearning.manymanybi.entity.User;
import com.greatlearning.manymanybi.repository.RoleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ManyManyBiApplicationTests {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    void saveRole() {
        User user = new User();
        user.setFirstName("krishna");
        user.setLastName("gupta");
        user.setEmail("krishna@gmail.com");
        user.setPassword("guptaji");

        User admin = new User();
        admin.setFirstName("admin1");
        admin.setLastName("admin2");
        admin.setEmail("admin1@gmail.com");
        admin.setPassword("admin3");

        Role roleAdmin = new Role();
        roleAdmin.setName("ROLE_ADMIN");

        roleAdmin.getUsers().add(user);
        roleAdmin.getUsers().add(admin);

        user.getRoles().add(roleAdmin);
        admin.getRoles().add(roleAdmin);

        roleRepository.save(roleAdmin);
    }

    @Test
    void fetchRole() {
        List<Role> roles = roleRepository.findAll();
        roles.forEach((r) -> {
            System.out.println(r.getName());
            r.getUsers().forEach((u) -> {
                System.out.println(u.getFirstName());
            });
        });
    }

}
